# -*- coding: utf-8 -*-
import time
import jwt


def valid_token(token):
    # 加载pub.key私钥文件
    pub_key = open("./pub.key").read()

    # 验证Token,并且在验证通过之后解析Payload，如果只是想解析Payload内容的话，可以将verify设置为false
    try:
        payload = jwt.decode(token, pub_key, verify=True, issuer="iam",algorithms='RS256')
        print {"message": "ok!"}, 200
    except Exception as e:
        print {"message": repr(e)}, 401
        return

    #输出 Payload
    print "iss:       ", payload["iss"]
    print "id:        ", payload["id"]
    print "exp:       ", payload["exp"]
    print "orig_iat:  ", payload["orig_iat"]
    print "user_id:   ", payload["user_id"]
    print "email:     ", payload["email"]
    print "user_type: ", payload["user_type"]

    print "expire:    ", payload['exp'] - time.time() < 0


if __name__ == '__main__':
    # Token示例
    valid_token(
        "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImNvbnN1bWVyQGsyZGF0YS5jb20uY24iLCJleHAiOjE1MjQ4ODUwNDIsImlkIjoiY29uc3VtZXJAazJkYXRhLmNvbS5jbiIsImlzcyI6ImlhbSIsIm9yaWdfaWF0IjoxNTI0ODg0NDQyLCJ1c2VyX2lkIjoidXNyLTY5NjE2ZGEwOTZmZDgxZWJiZWM1OTRhZGFiNTM3MWFhNzFmNzY3IiwidXNlcl90eXBlIjoiY29uc3VtZXIifQ.V4b-oN14Lm_gnrE_bccLVmUq6hqfbSocd7bQfw30mhvy1cSsA3M0-12s3MualRuBD42_MJLSyPb_StUGlp-hjw")
